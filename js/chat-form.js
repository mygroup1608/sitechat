(function ($, Drupal) {
  Drupal.behaviors.myModuleBehavior = {
    attach: function (context, settings) {
		jQuery("#form-chat-list").animate({ scrollTop: jQuery("#form-chat-list")[0].scrollHeight}, 0);
	
    }
  };
})(jQuery, Drupal);

jQuery(document).ready(function() {
	
	CKEDITOR.on('instanceReady',function(){
				console.log('ready')
				var id=jQuery('textarea[id^="edit-new-value"]').attr('id');		
				CKEDITOR.instances[id].setData('');
			});
	
	//search field 
	jQuery('body').on('keyup','.search-user-chat',function(){
		var search=jQuery('.search-user-chat').val();
		search_reg=new RegExp(search,'gi');
		console.log(search_reg);
		if(search.length>2) {
			jQuery('.user-chat-info-name').each(function(index,elem){
				var name=jQuery(elem).text();
			
				if(search_reg.test(name)) {
					jQuery(elem).closest('.custom-radio').show();
				} else {
					jQuery(elem).closest('.custom-radio').hide();
				}
				
				
			})
		} else {
			jQuery('.user-chat-form-wrapper .custom-radio').show();
		}
		
	})		
})