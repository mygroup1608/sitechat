<?php

namespace Drupal\sitechat\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\sitechat\Form\ChatList;

/**
 * Provides a 'Last Unread Messages' Block.
 *
 * @Block(
 *   id = "last_unread",
 *   admin_label = @Translation("Last Unread Messages"),
 *   category = @Translation("Last Unread Messages"),
 * )
 */
class LastUnread extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
	
	$current_uid=\Drupal::currentUser()->id();
	$user = \Drupal\user\Entity\User::load($current_uid);
	
	// 1. Create contact list
	$tr_list=\Drupal::service('private_message.mapper')->getThreadIdsForUser($user);
	
	
	$all_members=[];
	foreach($tr_list as $tr_id) {
		$thread_manager=\Drupal::entityTypeManager()->getStorage('private_message_thread');
		$thread=$thread_manager->load($tr_id);
		$membersIds=$thread->getMembersId();
		foreach($membersIds as $member_id) {
			if($current_uid!=$member_id) {
				$all_members[$member_id]=['uid'=>$member_id,'thread'=>$thread];
			}
		}
	}
	
	//theme contact lists
	$options=[];
	foreach ($all_members as $member_uid=>$member_data) {
		
		$th=\Drupal::service('sitechat.api')->get_user_info($member_data['uid'],$all_members[$member_uid]['thread']); 
		if(!$th) {
			continue;
		}
		$th['#new']=\Drupal::service('sitechat.api')->countNewMessages($all_members[$member_uid]['thread'],$user);
		
		if ($th['#new']) {
			$th['#theme']='user_chat_info_link';
			$th['#link']=\Drupal\Core\Url::fromRoute('sitechat.chat_list',array('user'=>$current_uid))->setOption('query',array('dialog_uid'=>$member_uid))->toString();
			//additional data
			$all_members[$member_uid]['img']=$th['#img'];
			$all_members[$member_uid]['name']=$th['#name'];			
			
			$member_html = \Drupal::service('renderer')->render($th);	
			$options[$member_data['uid']]=$member_html;
		}
		
		
	} 
	
	$html=implode('',$options);
    if(!count($options)) {
		$html=t('No unread messages');
	}
	return [
      '#markup' => $html,
	  '#attached'=>['library' => ['sitechat/sitechat-chat']]
    ];
  }
  
  
  
  public function getCacheMaxAge() {
		return 0;
	}

}