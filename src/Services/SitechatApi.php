<?php

namespace Drupal\sitechat\Services;

use Drupal\user\Entity\User;
use Drupal\image\Entity\ImageStyle;

/**
 * sitechat.api service
 */
class SitechatApi
{
	/**
	 * return number of new users messages (since last access)
	 */
	public function countNewMessages($thread, $user)
	{

		$time = $thread->getLastAccessTimestamp($user);

		$res = \Drupal::database()->query(
			'SELECT COUNT(message.id) FROM {private_messages} AS message ' .
				'JOIN {private_message_thread__private_messages} AS thread_message ' .
				'ON message.id = thread_message.private_messages_target_id ' .
				'JOIN {private_message_threads} AS thread ' .
				'ON thread_message.entity_id = thread.id ' .
				'JOIN {private_message_thread__members} AS thread_member ' .
				'ON thread_member.entity_id = thread.id AND thread_member.members_target_id = :uid ' .
				'JOIN {private_message_thread__last_access_time} AS last_access ' .
				'ON last_access.entity_id = thread.id ' .
				'JOIN {pm_thread_access_time} as access_time ' .
				'ON access_time.id = last_access.last_access_time_target_id AND access_time.owner = :uid AND access_time.access_time <= thread.updated ' .
				'WHERE thread.updated > :timestamp AND message.created > :timestamp AND message.owner <> :uid AND thread.id=:id',
			[
				':uid' => $user->id(),
				':timestamp' => $time,
				':id' => $thread->id()
			]
		)->fetchField();


		return $res;
	}

	/**
	 * user data for theme functions
	 */
	public function get_user_info($uid, $thread = false)
	{
		$th = [
			'#theme' => 'user_chat_info',
			'#img' => '',
			'#name' => '',
			'#online' => '',
			'#new' => ''
		];
		$member_user = User::load($uid);


		$member_profile = false;
		if (!$member_user) {
			return false;
		}


		if ($member_user) {
			if ($member_user->hasRole('dts_student')) {
				//student profile
				$member_profile = \Drupal::entityTypeManager()->getStorage('profile')->loadByUser($member_user, 'dts_student');
				if ($member_profile) {
					$th['#name'] = $member_profile->get('field_profile_real_first_name')->getString() . ' ' . $member_profile->get('field_profile_real_last_name')->getString();

					$field_profile_image = $member_profile->field_profile_image;
					if (is_object($field_profile_image) && !empty($field_profile_image->entity)) {
						$image_uri = $field_profile_image->entity->getFileUri();
						$style = ImageStyle::load('30x30');
						$url = $style->buildUrl($image_uri);
						$th['#img'] = $url;
					}
				}
			} elseif ($member_user->hasRole('dts_teacher')) {
				//provider profile
				$member_profile = \Drupal::entityTypeManager()->getStorage('profile')->loadByUser($member_user, 'dts_teacher');
				if ($member_profile) {

					$th['#name'] = $member_profile->get('field_profile_real_first_name')->getString() . ' ' . $member_profile->get('field_profile_real_last_name')->getString();
					$field_profile_image = $member_profile->field_profile_image;
					if (is_object($field_profile_image) && !empty($field_profile_image->entity)) {
						$image_uri = $field_profile_image->entity->getFileUri();
						$style = ImageStyle::load('30x30');
						$url = $style->buildUrl($image_uri);
						$th['#img'] = $url;
					}
				}
			}
		}

		if (!$member_profile) {
			//if no profiles - using account
			$th['#name'] = $member_user->getDisplayName();
		}

		$th['#online'] = false;

		return $th;
	}
}
