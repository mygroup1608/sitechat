<?php

namespace Drupal\sitechat\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\node\NodeInterface;
use Drupal\Core\Cache\CacheableRedirectResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ResponseSubscriber.
 *
 * @package Drupal\sitechat
 */
class ResponseSubscriber implements EventSubscriberInterface
{


  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * Constructor.
   */
  public function __construct(CurrentRouteMatch $current_route_match)
  {
    $this->routeMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    $events['kernel.response'] = ['handle'];

    return $events;
  }

  /**
   * This method is called whenever the kernel.response event is
   * dispatched.
   *
   * @param GetResponseEvent $event
   */
  public function handle(Event $event)
  {

    $route_name = $this->routeMatch->getRouteName();

    //redirect from default private massage module massages list to the new variant
    switch ($route_name) {
      case 'private_message.private_message_page':
        $current_uid = \Drupal::currentUser()->id();
        $url = \Drupal\Core\Url::fromRoute('sitechat.chat_list', ['user' => $current_uid])->toString();

        $event->setResponse(new RedirectResponse($url));
        break;
    }
  }
}
