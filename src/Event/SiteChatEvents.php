<?php

namespace Drupal\sitechat\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Sitechat eventss
 */
class SiteChatEvents extends Event
{


  const NEW_PRIVATE_MESSAGE = 'sitechat.new_private_message';

  /**
   * 
   */
  protected $message;
  protected $user;

  /**
   * constructor.
   */
  public function __construct($message, $user)
  {

    $this->message = $message;
    $this->user = $user;
  }

  /**
   * Returns data array 
   */
  public function getData()
  {

    return ['message' => $this->message, 'user' => $this->user];
  }
}
