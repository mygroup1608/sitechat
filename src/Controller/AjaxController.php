<?php

namespace Drupal\sitechat\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\Element\EntityAutocomplete;



/**
 * Provides route responses for the sitechat module.
 */
class AjaxController extends ControllerBase
{

	/**
	 * Returns names list for autocomplete.
	 *   
	 */
	public function autocomplete_names(Request $request)
	{
		$results = [];
		$name = $request->query->get('q');



		if (!$name) {
			return new JsonResponse($results);
		}

		$query = \Drupal::entityQuery('profile');
		$group = $query->orConditionGroup();
		//find users with entered name
		$group->condition('field_profile_real_first_name', $name, 'CONTAINS');
		$group->condition('field_profile_real_last_name', $name, 'CONTAINS');

		$query->condition($group);
		$profiles = $query->execute();
		$results = [];
		foreach ($profiles as $profile_id) {
			$profile = \Drupal::entityTypeManager()->getStorage('profile')->load($profile_id);
			$results[] = [
				'value' => $profile->get('field_profile_real_first_name')->getString() . ' ' . $profile->get('field_profile_real_last_name')->getString() . ' (' . $profile->getOwnerId() . ')',
				'label' => $profile->get('field_profile_real_first_name')->getString() . ' ' . $profile->get('field_profile_real_last_name')->getString()
			];
		}


		return new JsonResponse($results);
	}
}
