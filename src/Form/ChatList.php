<?php

namespace Drupal\sitechat\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Component\Utility\Html;
use Drupal\message\Entity\Message;
use Drupal\user\Entity\User;
use Drupal\private_message\Entity\PrivateMessage;


/**
 * Prodviding Chat page forms
 */
class ChatList extends FormBase
{

	/**
	 * Drupal\Core\Messenger\MessengerInterface definition.
	 *
	 * @var \Drupal\Core\Messenger\MessengerInterface
	 */
	protected $config;
	protected $userData;


	public function __construct()
	{
		$this->config = \Drupal::service('config.factory');
		$this->userData = \Drupal::service('user.data');
		$this->providerManager = \Drupal::service('video_embed_field.provider_manager');
		$current_uid = \Drupal::currentUser()->id();
		$this->currentUser = User::load($current_uid);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getFormId()
	{
		return 'sitechart_form';
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state)
	{

		$form['#prefix'] = '<div id="chat-list-form">';
		$form['#suffix'] = '</div>';
		$current_uid = \Drupal::currentUser()->id();
		$user = User::load($current_uid);

		// 1. List of active chats		
		$tr_list = \Drupal::service('private_message.mapper')->getThreadIdsForUser($user);

		$all_members = [];
		foreach ($tr_list as $tr_id) {
			$thread_manager = \Drupal::entityTypeManager()->getStorage('private_message_thread');
			$thread = $thread_manager->load($tr_id);
			$membersIds = $thread->getMembersId();
			foreach ($membersIds as $member_id) {
				if ($current_uid != $member_id) {
					$all_members[$member_id] = ['uid' => $member_id, 'thread' => $thread];
				}
			}
		}
		//no active users
		if (!count($all_members)) {
			$form['empty'] = ['#markup' => t('No messages')];
			$form['#onlybutton'] = true;
		}
		//theming users list
		$options = [];
		foreach ($all_members as $member_uid => $member_data) {

			$th = \Drupal::service('sitechat.api')->get_user_info($member_data['uid'], $all_members[$member_uid]['thread']);
			$th['#new'] = \Drupal::service('sitechat.api')->countNewMessages($all_members[$member_uid]['thread'], $user);

			$all_members[$member_uid]['img'] = '';
			if (isset($th['#img'])) {
				$all_members[$member_uid]['img'] = $th['#img'];
			}
			$all_members[$member_uid]['name'] = '';
			if (isset($th['#name'])) {
				$all_members[$member_uid]['name'] = $th['#name'];
			}


			$member_html = \Drupal::service('renderer')->render($th);
			$options[$member_data['uid']] = $member_html;
		}

		//no messages
		if (!count($options)) {
			$form['empty'] = ['#markup' => t('No messages')];
			$form['#theme'] = 'user_chat_form';
			$form['#onlybutton'] = true;
			$form['#attributes'] = ['class' => ['sitechart-form']];
			return $form;
		}

		$form['#onlybutton'] = false;


		//search window
		$form['search'] = [
			'#type' => 'textfield',
			'#attributes' => ['placeholder' => 'search', 'class' => ['search-user-chat']]
		];


		$form['uid'] = array(
			'#type' => 'radios',
			'#title' => '',
			'#options' => $options,
			'#ajax' => [
				'callback' => '::formAjaxCallback',
				'disable-refocus' => FALSE,
				'event' => 'change',
				'wrapper' => 'chat-list-form',
				'progress' => array('type' => 'none'),
			]
		);

		//current user data
		$th = \Drupal::service('sitechat.api')->get_user_info($current_uid);
		//дозаносим данные в итоговый массив
		$all_members[$current_uid]['img'] = $th['#img'];
		$all_members[$current_uid]['name'] = $th['#name'];

		//2. New massage save
		$dialog_uid = $form_state->getValue('uid');
		if (empty($dialog_uid)) {
			//GET
			$dialog_uid = \Drupal::request()->query->get('dialog_uid');
		}
		if (empty($dialog_uid)) {
			//sorting dialogs			
			if (count($options)) {
				$members_uids = array_keys($options);
				$dialog_uid = $members_uids[0];
				$form['uid']['#default_value'] = $dialog_uid;
			}
		}

		$form['uid']['#default_value'] = $dialog_uid;



		$dialog_thread = $all_members[$dialog_uid]['thread'];

		$new_message = $form_state->getValue('new');
		//last user dialog access
		$dialog_thread->updateLastAccessTime($user)->save();

		if (!empty($new_message['value'])) {
			$new_message_value = $new_message['value'];


			$message = PrivateMessage::create();
			$message->set('message', $new_message_value);
			$message->set('owner', $user);
			$message->save();
			$dialog_thread->addMessage($message);
			$dialog_thread->save();
		}


		//3. Show current dialog

		$thread_messages = $dialog_thread->getMessages();
		$mes_list = [];
		//format massagesІ
		foreach ($thread_messages as $m_k => $m) {
			$owner_uid = $m->getOwnerId();

			$mes_list[$m_k] = [];
			$mes_list[$m_k]['text'] = $m->getMessage();
			$this->embed_video($mes_list[$m_k]['text']);
			$mes_list[$m_k]['img'] = $all_members[$owner_uid]['img'];
			$mes_list[$m_k]['date'] = date('d.m.Y', $m->getCreatedTime());
			if ($mes_list[$m_k]['date'] == date('d.m.Y')) {
				//$mes_list[$m_k]['date']=t('Today');
			}
			if ($owner_uid == $current_uid) {
				$mes_list[$m_k]['class'] = 'owner-response';
				$mes_list[$m_k]['type'] = 'owner';
			} else {
				$mes_list[$m_k]['class'] = 'guest-response';
				$mes_list[$m_k]['type'] = 'response';
			}
		}

		$mes_render = [
			'#theme' => 'user_chat_messages',
			'#list' => $mes_list,
			'#prefix' => '<div id="form-chat-list">',
			'#suffix' => '</div>'
		];
		$mes_html = \Drupal::service('renderer')->render($mes_render);
		$form['wrapper'] = [
			'#type' => 'container',
			'#prefix' => '<div id="chat-list-form">',
			'#suffix' => '</div>'
		];
		$form['wrapper']['chat'] = ['#markup' => $mes_html];

		$form['wrapper']['new'] = [
			'#type' => 'text_format',
			'#title' => $this->t('Message'),
			'#format' => 'chat_format',
			'#allowed_formats' => array('chat_format'),

		];

		if (!empty($new_message['value'])) {
			$form['wrapper']['new']['#default_value'] = '';


			$form_state->setValue('new', '');
		}
		$form['wrapper']['save'] = [
			'#type' => 'button',
			'#value' => $this->t('Send'),
			'#ajax' => [
				'callback' => '::formAjaxCallback',
				'disable-refocus' => FALSE,
				'event' => 'click',
				'wrapper' => 'chat-list-form',
				'progress' => array('type' => 'none'),
			]
		];

		$form['#attached']['library'][] = 'sitechat/sitechat-chat';
		$form['#theme'] = 'user_chat_form';
		$form['#attributes'] = ['class' => ['sitechart-form']];
		return $form;
	}

	public function formAjaxCallback(array &$form, FormStateInterface $form_state)
	{
		unset($form['#attached']);
		return $form;
	}



	private function embed_video(&$text)
	{
		if (!preg_match_all('/(<p>)?(?<json>{(?=.*preview_thumbnail\b)(?=.*settings\b)(?=.*video_url\b)(?=.*settings_summary)(.*)})(<\/p>)?/', $text, $matches)) {
			return [];
		}
		$valid_matches = [];
		foreach ($matches['json'] as $delta => $match) {
			// Ensure the JSON string is valid.
			$embed_data = json_decode($match, TRUE);
			if (!$embed_data || !is_array($embed_data)) {
				continue;
			}
			$valid_matches[$matches[0][$delta]] = $embed_data;
		}

		foreach ($valid_matches as $source_text => $embed_data) {
			if (!$provider = $this->providerManager->loadProviderFromInput($embed_data['video_url'])) {
				continue;
			}

			$autoplay = $this->currentUser->hasPermission('never autoplay videos') ? FALSE : $embed_data['settings']['autoplay'];
			$embed_code = $provider->renderEmbedCode($embed_data['settings']['width'], $embed_data['settings']['height'], $autoplay);

			$embed_code = [
				'#type' => 'container',
				'#attributes' => [
					'class' => [Html::cleanCssIdentifier(sprintf('video-embed-field-provider-%s', $provider->getPluginId()))],
				],
				'children' => $embed_code,
			];

			// Add the container to make the video responsive if it's been
			// configured as such. This usually is attached to field output in the
			// case of a formatter, but a custom container must be used where one is
			// not present.
			if ($embed_data['settings']['responsive']) {
				$embed_code['#attributes']['class'][] = 'video-embed-field-responsive-video';
			}

			// Replace the JSON settings with a video.
			$text = str_replace($source_text, \Drupal::service('renderer')->render($embed_code), $text);

			// Add the required responsive video library only when at least one match
			// is present.
			//$response->setAttachments(['library' => ['video_embed_field/responsive-video']]);
			// $response->setCacheContexts(['user.permissions']);
		}
	}


	/**
	 * {@inheritdoc}
	 */
	public function AjaxCallback(array &$form, FormStateInterface $form_state)
	{
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state)
	{
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state)
	{
	}
}
